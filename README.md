# Basic emacs according to me
This is not my main emacs config these are my go to packages, my must haves on any emacs install that I work in.

I've put them all together here so that I can pull them onto new machines when I need to get started.

It uses
+ evil; for vim editing
+ magit; because it's the best
+ helm; maybe ivy is better I don't know
+ ace-window and ace-jump; These are ingrained in my muscle memory
+ paredit; because how could you not?
+ and finally company mode again is it the best? I don't know
