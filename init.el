(require 'package)

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)
(package-refresh-contents t)

(scroll-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)

(setq make-backup-files nil)

;; Use only spaces (no tabs at all).
(setq-default indent-tabs-mode nil)

;; Always show column numbers.
(setq-default column-number-mode t)

(set-face-attribute 'default nil
                    :family "firacode"
                    :foundry "*"
                    :height 110 ;; units of 1/10pt;  110 = 11pt 
                    :width 'normal)


;; ------------ packages ---------------
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(setq use-package-always-ensure t)

(use-package undo-tree
  :ensure t
  :config
  (global-undo-tree-mode))

(use-package evil
  :ensure t
  :demand t
  :init
  (setq evil-want-keybinding nil)
  (setq evil-want-C-i-jump nil)
  (global-evil-leader-mode)
  :config 
  (setq evil-undo-system 'undo-tree) ;; Evil is so smart that it can ignore this
  (evil-global-set-key 'normal "u" 'undo-tree-undo)
  (evil-global-set-key 'normal (kbd "C-r") 'undo-tree-redo)
  (setq evil-auto-indent -1)
  (evil-mode 1))

(use-package evil-collection
  :ensure t
  :config
  (mapc  #'evil-collection-init 
         '(dired calendar markdown-mode info yaml-mode)))

(use-package magit
  :ensure t
  :bind (("C-x g" . magit)))

(use-package company
  :ensure t
  :config (global-company-mode))

(use-package helm
  :ensure t
  :after company
  :bind (("C-x C-f" . helm-find-files)
         ("M-x" . helm-M-x))
  :config
  (helm-mode))

(use-package paredit
  :ensure t
  :config
  (add-hook 'emacs-lisp-mode-hook 'paredit-mode)
  (add-hook 'emacs-lisp-mode-hook       'paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook 'paredit-mode)
  (add-hook 'ielm-mode-hook             'paredit-mode)
  (add-hook 'lisp-mode-hook             'paredit-mode)
  (add-hook 'lisp-interaction-mode-hook 'paredit-mode)
  (add-hook 'scheme-mode-hook           'paredit-mode))

(use-package ace-jump-mode
  :ensure t
  :config
  (autoload
    'ace-jump-mode-pop-mark
    "ace-jump-mode"
    "Ace jump back:-)"
    t)
  (ace-jump-mode-enable-mark-sync)
  :bind
  ("C-x SPC" . evil-ace-jump-word-mode))

(use-package ace-window
  :ensure t
  :bind
  ("M-o" . ace-window))
